'''input'''
'''
# legg sammen to tall fra bruker og skriv ut: 
a = int(input('Skriv inn et tall: '))
b = int(input('Skriv inn enda et tall: '))
c = a+b
print('Summen blir: ', c)
'''


# legge sammen to tall fra bruker, og gjøre om
#til streng igjen og skrive ut
tall1 = int(input('Tall 1: '))
tall2 = int(input('Tall 2: '))
summen = tall1 + tall2

string = str(summen)

print('Summen blir: ' + string)