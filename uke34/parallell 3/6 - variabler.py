'''VARIABLER'''

# opprette variabler, lag en variabel med et tall
tall = 4

# lag en variabel med tekst
tekst = 'min tekst'

# lag en variabel med et kommatall
desimal = 6.2731

# legge sammen variabler
total = tall+desimal
print(total)
print(tekst*tall)
